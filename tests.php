<?php

require("StringCalculator.php");

// Test the Add() method in StringCalculator.
// Key -> input and Value -> expected output
$tests = array(
	""			=>	0,
	"hello"		=>	0,
	"he11o"		=> 	0,
	","			=>	0,
	"1,"		=>	1,
	",1"		=>	1,
	"1"			=>	1,
	"1,2"		=>	3,
	"1,2,5"		=>	8,
	",,1,,,2,5"	=>	8,
	"0,0,,,1"	=>	1,

	'1\n,2,3'	=>	6,
	'1,\n2,4'	=>	7,
	'\n'		=>	0,
	'\nhello\n'	=>	0,
	'\nhe11o\n'	=>	0,
	'\nhe,\n1'	=>	1,
	'\n,,\n1'	=>	1,
	'\n\n\n\n'	=>	0,

	'//;\n1;3;4'	=>	8,
	'//$\n1$2$3'	=>	6,
	'//@\n2@3@8'	=>	13,
	'//&\n&1&1&1&1'	=>	4,
	'//"\n5"2"1"'	=>	8,
	"//'\\n6'2'1"	=>	9,
	'///\n3/4/5'	=>	12,

	"-2,5,-3"		=>	NULL,
	"5,2,45,-6,1"	=>	NULL,
	'//;\n101;-1;0'	=>	NULL,
	'//%\n-4,-4,-4' =>	NULL,
	'-0,-0,1,2'		=>	3,
	'-2,-1,-0,1,2'	=>	NULL,
	'-0,-0,-0'		=>	0,
	'//;\n1;-0;1'	=>	2,
	'100,100,-200'	=>	NULL,
	'-100,-3,100'	=>	NULL,

	"1,1001"		=> 1,
	"1,1000"		=> 1001,

	'//***\n1***2***3' 		=> 6,
	'/////\n0///1///3' 		=> 4,
	'//$%$\n5$%$2$%$3' 		=> 10,
	'//"\n1000"2000"1000' 	=> 2000,

	'//$,@\n1$2@3'			=> 6,
	'//$$$,@\n1$$$2@5' 		=> 8,
	'//$$,!@\n10$$1!@2' 	=> 13,
	'//,\n1,1,2' 	=> 4,

);

foreach ($tests as $input => $output) {
	echo "Testing Add(\"$input\"), Expecting: $output ...";
	
	try {
		$returned = StringCalculator::Add((string)$input);
		
		if ($returned === $output) {
			echo "passed\n";	
		} else {
			echo "*** failed *** -> Actual: $returned \n";
		}
	} catch (Exception $e) {
		echo "*** EXCEPTION *** -> " . $e->getMessage() . "\n"; 
	}
}
