<?php

class StringCalculator {

	/**
	 * Takes a string of non-negative numbers delimited by a comma and adds them together.
	 * This method will automatically parse newline character strings.
	 *
	 * You may also provide a custom delimiter by using the format //?\nZ?Z?Z?Z
	 * Where (?) represents the delimiter and (Z) represents a sequence of integers.
	 * Similiarily multiple delimiters may also be specified by a comma.
	 * 
	 * Numbers larger than 1000 are ignored.
	 *
	 * @param number : string of delimited numbers
	 * @return int : the sum of the integers
	 */
	public static function Add($number) {

		// Ensure the provided param is in fact a string
		if (gettype($number) !== "string") {
			throw new Exception("number must be of type string!");
		}

		// Ensure no negatives are found in the provided string
		preg_match_all('~\\-[1-9][0-9]*~', $number, $matches, PREG_SET_ORDER);
		if (!(empty($matches))) {
			$sb_negatives = "";
			foreach ($matches as $match) {
				$sb_negatives .= $match[0] . " ";
			}
			throw new Exception("Negatives not allowed! [ $sb_negatives ]");
		}

		// Determine whether or not a custom delimiter format is provided.
		if (preg_match('~^\\/{2}.+\\\n.+$~', $number)) {
			$number = explode('\n', $number);

			$delims = explode(",", substr($number[0], 2));
			foreach ($delims as $delim) {
				$number[1] = str_replace($delim, ",", $number[1]);
			}

			$number = explode(",", $number[1]);
		} else {
			$number = explode(",", $number);
		}

		// Actually perform the calculation
		$count = 0;
		foreach ($number as $num) {
			$val = (int)trim($num, '\n');
			if($val <= 1000) {
				$count += $val;
			}
		}
		return $count;
	}
}
